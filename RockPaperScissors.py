import random

print("ROCK ,PAPER ,SCISSORS")
end_of_game = False

# select_array = ['Rock', 'Paper', 'Scissors']
sel_dict = {'r': 'Rock', 'p': 'Paper', 's': 'Scissors', '':''}
lst = ['r', 'p', 's']
ties = 0
wins = 0
loss = 0
while not end_of_game:
    response = input("Enter your move :(r)ock (p)aper (s)cissors (q)uit")
    if response != 'q':
        computer_choice = random.choice(lst)
        print(computer_choice)
        if response == lst[0] and computer_choice == lst[1]:
            print(f"{sel_dict[response]} versus {sel_dict[computer_choice]}")
            print(f"You lose!")
            loss += 1
            print(f"{wins} Wins , {loss} losses , {ties} ties")

        elif response == lst[0] and computer_choice == lst[2]:
            print(f"{sel_dict[response]} versus {sel_dict[computer_choice]}")
            print(f"You win!")
            wins += 1
            print(f"{wins} Wins , {loss} losses , {ties} ties")
        elif response == lst[1] and computer_choice == lst[0]:
            print(f"{sel_dict[response]} versus {sel_dict[computer_choice]}")
            print(f"You win!")
            wins += 1
            print(f"{wins} Wins , {loss} losses , {ties} ties")

        elif response == lst[1] and computer_choice == lst[2]:
            print(f"{sel_dict[response]} versus {sel_dict[computer_choice]}")
            print(f"You lose!")
            loss += 1
            print(f"{wins} Wins , {loss} losses , {ties} ties")

        elif response == lst[2] and computer_choice == lst[0]:
            print(f"{sel_dict[response]} versus {sel_dict[computer_choice]}")
            print(f"You lose!")
            loss += 1
            print(f"{wins} Wins , {loss} losses , {ties} ties")
        elif response == lst[2] and computer_choice == lst[1]:
            print(f"{sel_dict[response]} versus {sel_dict[computer_choice]}")
            print(f"You win!")
            wins += 1
            print(f"{wins} Wins , {loss} losses , {ties} ties")
        else:
            print(f"{sel_dict[response]} versus {sel_dict[computer_choice]}")
            print(f"Its a tie")
            ties += 1
            print(f"{wins} Wins , {loss} losses , {ties} ties")
    else:
        print("Please enter proper command  and play again")
        print("Thanks for playing")
        print(f"Final score :{wins} Wins , {loss} losses , {ties} ties")
        end_of_game = True
