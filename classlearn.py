class Dog:
    species = "rare"

    def __init__(self, name, age):
        self.name = name 
        self.age = age 

    def describe(self):
        return f"{self.name} is {self.age} years old"
    
    def __str__(self):
        return f"{self.name} is {self.age} years old"

    
    def speak(self, sound):
        return f"{self.name} says {sound}"


pixel = Dog('Saurav','40')

print(pixel.speak('Bow Bow'))

print(pixel.describe())

# print(pixel)

# # miles = Dog("Miles", 4, "Jack Russell Terrier")
# # buddy = Dog("Buddy", 9, "Dachshund")
# # jack = Dog("Jack", 3, "Bulldog")
# # jim = Dog("Jim", 5, "Bulldog")




class JackRussellTerrier(Dog):
    def speak(self, sound ="Arf "):
        print(f"{self.name} saaaaaays {sound}")

class Dachshund(Dog):
    pass

class Bulldog(Dog):
    pass


miles = JackRussellTerrier("Miles",4)

print(miles.speak())


if isinstance(miles, Dog):
    print("Miles is indeed an instance of Dog. ")
else:
    print("No luck!")



class Employee:

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
        self.salary = "500000"
        self.gender = "Male"
        self.location = "Mumbai"
        ...

    def get_full_name_with_location(self):
        return f"{self.first_name} {self.last_name}, {self.location}, Salary : {self.salary}"

    def get_nothing(self):
        pass
    
    def get_something(self):
        return
        

self = Employee("Vaishak", "Nambiar") #set

print(self.get_full_name_with_location())

#Java Method
# Employee employee = new Employee();
# employee.getAllDetails();
